﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace SecurityStoryXXE.Controllers
{
	public class HomeController : Controller
	{
		public HomeController()
		{
		}

		public IActionResult Index()
		{
			return View();
		}

		public IActionResult XmlDocumentTest()
		{
			ViewData["Title"] = "XmlDocument Page";

			string xmlContent = GetXmlContent();

			XmlDocument xmlDoc = new XmlDocument();
			// Setting this to NULL disables DTDs and helps to avoid injection
			xmlDoc.XmlResolver = new XmlUrlResolver();
			xmlDoc.LoadXml(xmlContent);

			ViewData["xml-content"] = xmlDoc.InnerText;

			return View("View");
		}

		public IActionResult XPathNavigatorTest()
		{
			ViewData["Title"] = "XPathNavigator Page";

			string xmlContent = GetXmlContent();
			byte[] bytes = Encoding.UTF8.GetBytes(xmlContent);
			using (Stream stream = new MemoryStream(bytes))
			{
				XmlTextReader xmlReader = new XmlTextReader(stream);
				// Setting this to NULL disables DTDs and helps to avoid injection
				xmlReader.XmlResolver = new XmlUrlResolver();
				XPathDocument doc = new XPathDocument(xmlReader);
				XPathNavigator nav = doc.CreateNavigator();
				ViewData["xml-content"] = nav.InnerXml.ToString();
			}

			return View("View");
		}

		private string GetXmlContent()
		{
			string pathToAnyFile = Path.Combine(Environment.CurrentDirectory, "testdata.txt");
			string xxePayload = "<?xml version='1.0' ?>" +
				$"<!DOCTYPE doc [<!ENTITY win SYSTEM 'file:///{pathToAnyFile}'>]>" +
				"<doc>" +
					"<filecontent>&win;</filecontent>" +
					"<xtext>[Some other text]</xtext>" +
				"</doc>";
			return xxePayload;
		}

		private void XmlReaderView()
		{
			string xmlContent = GetXmlContent();
			byte[] bytes = Encoding.UTF8.GetBytes(xmlContent);
			using (Stream stream = new MemoryStream(bytes))
			{
				XmlTextReader reader = new XmlTextReader(stream);
				// NEEDED because the default is FALSE in versions prior to .NET 4.0!!
				reader.ProhibitDtd = true;

				// ...

				reader = new XmlTextReader(stream);
				// NEEDED because the default is Parse in versions .NET 4.0 - .NET 4.5.2!!
				reader.DtdProcessing = DtdProcessing.Prohibit;
			}
		}
	}
}
